package watercalc;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;



@DisplayName("WaterCalculator")
public class WaterCalculatorTest
{
    WaterCalculator waterCalculator = new WaterCalculator();

    @Test
    @DisplayName("should return 0 on empty landscape")
    public void shouldReturnZeroOnEmptyLandscape() {
        assertEquals(
            0,
            waterCalculator.calculateWaterAmount(new int[]{}));
    }


    @Test
    @DisplayName("should return 0 when all peaks are the same height")
    public void shouldReturnZeroIfAllPeaksAreTheSameHeight() {
        int[] landscape = {100, 100, 100, 100, 100};

        assertEquals(
            0,
            waterCalculator.calculateWaterAmount(landscape));
    }


    @Test
    @DisplayName("should return 0 when there is only one peak at center")
    public void shouldReturnZeroIfThereIsOnlyOnePeak() {
        int[] landscape = {0, 1, 2, WaterCalculator.MAX_PEAK, 2, 1, 0};

        assertEquals(
            0,
            waterCalculator.calculateWaterAmount(landscape));
    }


    @Test
    @DisplayName("should throw exception if some peak is too high")
    public void shouldThrowExceptionIfPeakTooHigh() {
        assertEquals(0,
            waterCalculator.calculateWaterAmount(
                new int[]{0, 1, 2, WaterCalculator.MAX_PEAK, 2, 1, 0}));

        assertThrows(IllegalArgumentException.class,
            () -> waterCalculator.calculateWaterAmount(
                new int[]{0, 1, 2, WaterCalculator.MAX_PEAK + 1, 2, 1, 0}));
    }


    @Test
    @DisplayName("should throw exception if the landscape is too big")
    public void shouldThrowExceptionIfLandscapeIsTooBig() {
        assertEquals(0,
            waterCalculator.calculateWaterAmount(
                new int[WaterCalculator.MAX_LANDSCAPE]));

        assertThrows(
            IllegalArgumentException.class,
            () -> waterCalculator.calculateWaterAmount(
                new int[WaterCalculator.MAX_LANDSCAPE + 1]));
    }

    @Test
    @DisplayName("should properly calculate the amount of water on dataset 1")
    public void shouldCalculateTheAmountOfWaterOnDataset1() {
        int[] landscape = {WaterCalculator.MAX_PEAK, 0, 0, 0, WaterCalculator.MAX_PEAK};

        assertEquals(
            WaterCalculator.MAX_PEAK * 3,
            waterCalculator.calculateWaterAmount(landscape));
    }

    @Test
    @DisplayName("should properly calculate the amount of water on dataset 2")
    public void shouldCalculateTheAmountOfWaterOnDataset2() {
        int[] landscape = {WaterCalculator.MAX_PEAK, 3, 2, 1, 2, 3, 5, 3, 1};

        assertEquals(
            14,
            waterCalculator.calculateWaterAmount(landscape));
    }


    @Test
    @DisplayName("should properly calculate the amount of water on dataset 3")
    public void shouldCalculateTheAmountOfWaterOnDataset4() {
        int[] landscape = {0, 2, 1, 3, 0, 1, 0, 3, 1, 2, 0, 1};

        assertEquals(
            11,
            waterCalculator.calculateWaterAmount(landscape));
    }
}
