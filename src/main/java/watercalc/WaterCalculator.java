package watercalc;


public class WaterCalculator
{
  static final int MAX_LANDSCAPE = 32000;
  static final int MAX_PEAK = 32000;

  public long calculateWaterAmount(int[] landscape)
  {
    if (landscape.length > MAX_LANDSCAPE)
      throw new IllegalArgumentException("Landscape size out of range");

    int leftPeak = 0;
    int rightPeak = 0;
    long volume = 0;

    int leftIndex = 0;
    int rightIndex = landscape.length - 1;

    while (leftIndex < rightIndex) {
      if (landscape[leftIndex] > MAX_PEAK || landscape[leftIndex] < 0
          || landscape[rightIndex] > MAX_PEAK || landscape[rightIndex] < 0)
        throw new IllegalArgumentException("Landscape peak size out of range");

      if (landscape[leftIndex] < landscape[rightIndex]) {

        if (landscape[leftIndex] > leftPeak)
          leftPeak = landscape[leftIndex];
        else
          volume += leftPeak - landscape[leftIndex];

        leftIndex++;
      }
      else {

        if (landscape[rightIndex] > rightPeak)
          rightPeak = landscape[rightIndex];
        else
          volume += rightPeak - landscape[rightIndex];

        rightIndex--;
      }
    }

    return volume;
  }
}
